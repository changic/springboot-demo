package jm.springboot.demo.controller;

import jm.springboot.demo.model.Member;
import jm.springboot.demo.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author jm
 */
@Controller
public class MemberController {

    @Autowired
    private MemberService memberService;

    @GetMapping("members")
    public String members(ModelMap modelMap) {
        List<Member> memberList = memberService.getAll();
        modelMap.addAttribute("memberList", memberList);
        return "members";
    }

}
