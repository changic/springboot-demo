package jm.springboot.demo.service;

import jm.springboot.demo.model.Member;

import java.util.List;

public interface MemberService {

    public Member getById(Integer id);

    public List<Member> getByCondition(Member condition);

    public List<Member> getAll();
}
