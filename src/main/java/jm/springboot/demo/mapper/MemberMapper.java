package jm.springboot.demo.mapper;

import jm.springboot.demo.model.Member;
import tk.mybatis.mapper.common.Mapper;

public interface MemberMapper extends Mapper<Member> {
}